<?php
 include('../header.php');
?>

<script>
    function validate()
    {
      if(document.login_details.login_id.value == "")
      {
        alert("Please Enter Your Login ID to Login Your Account.");
        document.login_details.login_id.focus();
        return false;
      }
      if(document.login_details.password.value == "")
      {
        alert("Please Enter Your Login Password to Login Your Account.");
        document.login_details.password.focus();
        return false;
      }
    }
</script>
<div class="space"><span style="color: green; font-weight: bold; font-size: 17px;">Your User ID or Password Not Currect, Please Try Again</span></div>
<div id="main_reg_login">
 <div class="sub_main_space">
   
 <form action="../otp/login.php" method="POST" name="login_details" "enctype = "multipart/form-data" onsubmit="return validate()">
  <table width="80%" border="0px" align="center" cellspacing="0px" cellpadding="0px">
    <tr>
      <td class="label"><label for="login_id">Login ID</label></td>
    </tr>
    <tr>
      <td align="center"><input type="text" name="login_id" class="textbox_" placeholder="Your Login ID" id="login_id"></td>
    </tr>
    <tr>
      <td class="label"><label for="password">Password</label></td>
    </tr>
    <tr>
      <td align="center"><input type="password" name="password" class="textbox_" placeholder="Password" id="password"></td>
    </tr>
    <tr>
      <td align="center"><input type="submit" name="submit" class="button" value=" Log in "></td>
    </tr>
  </table>
 </form>
 </div>
</div>