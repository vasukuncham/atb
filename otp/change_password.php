<?php
 session_start();
 if(!$_SESSION['donor'])
 {
  print "<script>";
  print "self.location = 'logout.php'";
  print "</script>";
 }

 $donor_id = $_SESSION['donor'];

 include('menu_header.php');
 include('../config.php');

 $select = "SELECT * FROM atbblooddonar WHERE ATB_D_NO='$donor_id'";
 $run    = mysql_query($select);
 $fetch  = mysql_fetch_array($run);
 $p_pass = $fetch['Password'];

  // echo "Password is:".$p_pass;

?>

  <script>

    function Focus()
    {
      document.change_pass.e_password.focus();
    }

    var oldpass = "<?php echo $p_pass; ?>"

    function validate()
    {
      if(document.change_pass.e_password.value=="")
      {
        alert("Please Enter Existing Password");
        document.change_pass.e_password.focus();
        return false;
      }
      if(document.change_pass.n_password.value=="")
      {
        alert("Please Enter New Password");
        document.change_pass.n_password.focus();
        return false;
      }
      if(document.change_pass.r_n_password.value=="")
      {
        alert("Please Re-Enter New Password");
        document.change_pass.r_n_password.focus();
        return false;
      }
      if(document.change_pass.e_password.value!=oldpass)
      {
        alert("Old Password is not Correct");
        document.change_pass.e_password.focus();
        return false;
      }
      if(document.change_pass.n_password.value!=document.change_pass.r_n_password.value)
      {
        alert("Confirm Password dose not match");
        document.change_pass.r_n_password.focus();
        return false;
      }
    }
  </script>

<div id="main_reg_login">

      <table align="center" width="100%" border="0px"> 
        <tr>
          <td align="right"><a href = "#" onClick="history.go(-1)" class="home_">Back</a></td>
        </tr>
      </table>

 <div class="sub_main_space">
 <form action="../otp/change_password_load.php" method="POST" name="change_pass" "enctype = "multipart/form-data" onsubmit="return validate()">
  <table width="80%" border="0px" align="center" cellspacing="0px" cellpadding="0px">
    <tr>
      <td class="label"><label for="existing_password">Enter Existing Password</label></td>
    </tr>
    <tr>
      <td align="center"><input type="password" name="e_password" class="textbox_" placeholder="Your Existing Password" id="existing_password"></td>
    </tr>
    <tr>
      <td class="label"><label for="new_password">New Password</label></td>
    </tr>
    <tr>
      <td align="center"><input type="password" name="n_password" class="textbox_" placeholder="New Password" id="new_password"></td>
    </tr>
    <tr>
      <td class="label"><label for="re_new_password">Re-Enter New Password</label></td>
    </tr>
    <tr>
      <td align="center"><input type="password" name="r_n_password" class="textbox_" placeholder="Re-Enter New Password" id="re_new_password"></td>
    </tr>
    <tr>
      <td align="center"><input type="submit" name="submit" class="button" value=" Change Password "></td>
    </tr>
  </table>
 </form>
 </div>
</div>


<?php
  include('../footer.php');
?>