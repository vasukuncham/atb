<?php
 session_start();
 if(!isset($_SESSION['donor']))
 {
  print "<script>";
  print "self.location = 'logout.php'";
  print "</script>";
 }

 $donor_id = $_SESSION['donor'];

 include('menu_header.php');
 include('../config.php');

 $c_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];

 //$c_url = 'http'.($_SERVER['HTTPS']?'s':null).'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

 $select_state = "SELECT State_ID,State_Name FROM state";
 $run_state    = mysql_query($select_state);

 $select = "SELECT * FROM atbblooddonar WHERE ATB_D_NO='$donor_id'";
 $run    = mysql_query($select);
 $fetch  = mysql_fetch_array($run);

 $motivate_person = $fetch['Motivated_Person'];
 // echo "Motivative Person ID: ".$motivate_person."<br />";

 $select_mtv = "SELECT * FROM atbblooddonar WHERE ATB_D_NO='$motivate_person'";
 $run_mtv    = mysql_query($select_mtv);
 $fetch_mtv  = mysql_fetch_array($run_mtv);
 // echo "Motivative Person Name: ".$fetch_mtv['NAME']."<br />";

 

 if($fetch['Picture'] == "logo.jpg")
 {
  $path = "";
 }
 else
 {
  $path = "images/";
 }

 if($fetch['GENDER'] == "Male")
 {
  $person = "Mr. ";
 }
 else if($fetch['GENDER'] == "FeMale")
 {
  $person = "Mrs. ";
 }
?>
  <style type="text/css">
    @import "../css/jquery.datepick.css";
  </style>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.datepick.js"></script>

  <script type="text/javascript">
    $(function()
    {
      $('#popupDatepicker').datepick();
      $('#popupDatepicker_01').datepick();
      $('#inlineDatepicker').datepick({onSelect: showDate});
    }
    );
  </script>

  <script>

    function validate()
    {
      if(document.mod_details.m_name.value == "")
      {
        alert("Please Enter the Your Full Name");
        document.mod_details.m_name.focus();
        return false;
      }
      if(document.mod_details.m_dob.value == "")
      {
        alert("Please Select Your Date of Birth");
        document.mod_details.m_dob.focus();
        return false;
      }
/*
      if(document.register.gender[0].checked == "false" || document.register.gender[1].checked != "false")
      {
        alert("Please Select Your Gender");
        document.register.gender.focus();
        return false;
      }
*/
      if(document.mod_details.m_blood_group.value == "Select")
      {
        alert("Please Select Your Blood Group");
        document.mod_details.m_blood_group.focus();
        return false;
      }
      if(document.mod_details.m_phone.value == "")
      {
        alert("Please Enter Your Phone Number");
        document.mod_details.m_phone.focus();
        return false;
      }
      if(document.mod_details.m_location.value == "Select")
      {
        alert("Please Select Your Nearest Location");
        document.mod_details.m_location.focus();
        return false;
      }
    }
  </script>
  <script>
        function getDistrict(stateID){
        var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            var res=xmlhttp.responseText;
            // alert(res);
            document.getElementById("district").innerHTML=res;
            }
          }
        xmlhttp.open("GET","../registrations/locations_script.php?stateID="+stateID+"&ref=district", true);
        xmlhttp.send();
        }

        function getCities(cityID){
        var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            var res=xmlhttp.responseText;
            // alert(res);
            document.getElementById("city").innerHTML=res;
            }
          }
        xmlhttp.open("GET","../registrations/locations_script.php?cityID="+cityID+"&ref=city", true);
        xmlhttp.send();
        }

</script>

<div id="main_reg">

      <table align="center" width="100%" border="0px"> 
        <tr>
          <td align="right"><a href = "#" onClick="history.go(-1)"class="home_">Back</a></td>
        </tr>
      </table>

 <div class="sub_main_space">
  <form action="../otp/modify_details.php" method="POST" name="mod_details" enctype = "multipart/form-data" onsubmit="return validate()">
  <table width="95%" align="center" border="0px" cellspacing="0px" cellpadding="0px">
    <tr>
      <td width="30%" colspan="2" rowspan="3"><img src="<?php echo $path.$fetch['Picture']; ?>" width="150px" class="image_border"></td>
      <td width="70%" valign="center" class="donor_quetation">I want to chage my status</td>
    </tr>
    <tr>
      <td width="70%" valign="center" class="donor_quetation">
       <?php
         if($fetch['Status'] == "Active")
         {
           $p_status_01 = "Active";
           $p_status_02 = "On-Hold";
           $p_status_03 = "Delete";
         }
         if($fetch['Status'] == "On-Hold")
         {
           $p_status_01 = "On-Hold";
           $p_status_02 = "Active";
           $p_status_03 = "Delete";
         }
         if($fetch['Status'] == "Delete")
         {
           $p_status_01 = "Delete";
           $p_status_02 = "Active";
           $p_status_03 = "On-Hold";
         }
       ?>
        <select name = "status" id="status"class="textbox_">
         <option value="<?php echo $p_status_01; ?>"><?php echo $p_status_01; ?></option>
         <option value="<?php echo $p_status_02; ?>"><?php echo $p_status_02; ?></option>
         <option value="<?php echo $p_status_03; ?>"><?php echo $p_status_03; ?></option>
        </select>
      </td>
    </tr>
    <tr>
      <td width="70%" valign="bottom" class="donor_quetation">Modify Your Picture <input type="file" name="file"></td>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">Name</label></td>
      <td width="4%" class="static_head" align="left">--</td>
      <td width="72%" class="dynamic_values">
          <input type="text" name="m_name" value="<?php echo $fetch['NAME']; ?>" class="textbox_" id="name"></td>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">Your Age</label></td>
      <td width="4%" class="static_head" align="center">--</td>
      <td width="72%" class="dynamic_values">
          <input type="text" name="m_dob" value="<?php echo $fetch['AGE']; ?>" class="textbox_" id="popupDatepicker_"></td>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">Blood Group</label></td>
      <td width="4%" class="static_head" align="center">--</td>
      <td width="72%" class="dynamic_values">


       <?php

         if($fetch['BLOOD_GROUP'] == "A+")
         {
           $p_group_01 = "A+";
           $p_group_02 = "A-";
           $p_group_03 = "B+";
           $p_group_04 = "B-";
           $p_group_05 = "AB+";
           $p_group_06 = "AB-";
           $p_group_07 = "O+";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A-")
         {
           $p_group_01 = "A-";
           $p_group_02 = "A+";
           $p_group_03 = "B+";
           $p_group_04 = "B-";
           $p_group_05 = "AB+";
           $p_group_06 = "AB-";
           $p_group_07 = "O+";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "B+")
         {
           $p_group_01 = "B+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B-";
           $p_group_05 = "AB+";
           $p_group_06 = "AB-";
           $p_group_07 = "O+";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "B-")
         {
           $p_group_01 = "B-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "AB+";
           $p_group_06 = "AB-";
           $p_group_07 = "O+";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "AB+")
         {
           $p_group_01 = "AB+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB-";
           $p_group_07 = "O+";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "AB-")
         {
           $p_group_01 = "AB-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "O+";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "O+")
         {
           $p_group_01 = "O+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O-";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "O-")
         {
           $p_group_01 = "O-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "A1+";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A1+")
         {
           $p_group_01 = "A1+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1-";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A1-")
         {
           $p_group_01 = "A1-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A2+";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A2+")
         {
           $p_group_01 = "A2+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2-";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A2-")
         {
           $p_group_01 = "A2-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2+";
           $p_group_13 = "A1B+";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A1B+")
         {
           $p_group_01 = "A1B+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2+";
           $p_group_13 = "A2-";
           $p_group_14 = "A1B-";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A1B-")
         {
           $p_group_01 = "A1B-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2+";
           $p_group_13 = "A2-";
           $p_group_14 = "A1B+";
           $p_group_15 = "A2B+";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A2B+")
         {
           $p_group_01 = "A2B+";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2+";
           $p_group_13 = "A2-";
           $p_group_14 = "A1B+";
           $p_group_15 = "A1B-";
           $p_group_16 = "A2B-";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "A2B-")
         {
           $p_group_01 = "A2B-";
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2+";
           $p_group_13 = "A2-";
           $p_group_14 = "A1B+";
           $p_group_15 = "A1B-";
           $p_group_16 = "A2B+";
           $p_group_17 = "BB";
         }
         if($fetch['BLOOD_GROUP'] == "BB")
         {
           $chan_value = "BOMBAY BLOOD GROUP";

           $p_group_01 = $chan_value;
           $p_group_02 = "A+";
           $p_group_03 = "A-";
           $p_group_04 = "B+";
           $p_group_05 = "B-";
           $p_group_06 = "AB+";
           $p_group_07 = "AB-";
           $p_group_08 = "O+";
           $p_group_09 = "O-";
           $p_group_10 = "A1+";
           $p_group_11 = "A1-";
           $p_group_12 = "A2+";
           $p_group_13 = "A2-";
           $p_group_14 = "A1B+";
           $p_group_15 = "A1B-";
           $p_group_16 = "A2B+";
           $p_group_17 = "A2B-";
         }
       ?>
        <select name = "b_group" id="group"class="textbox_">
         <option value="<?php echo $p_group_01; ?>"><?php echo $p_group_01; ?></option>
         <option value="<?php echo $p_group_02; ?>"><?php echo $p_group_02; ?></option>
         <option value="<?php echo $p_group_03; ?>"><?php echo $p_group_03; ?></option>
         <option value="<?php echo $p_group_04; ?>"><?php echo $p_group_04; ?></option>
         <option value="<?php echo $p_group_05; ?>"><?php echo $p_group_05; ?></option>
         <option value="<?php echo $p_group_06; ?>"><?php echo $p_group_06; ?></option>
         <option value="<?php echo $p_group_07; ?>"><?php echo $p_group_07; ?></option>
         <option value="<?php echo $p_group_08; ?>"><?php echo $p_group_08; ?></option>
         <option value="<?php echo $p_group_09; ?>"><?php echo $p_group_09; ?></option>
         <option value="<?php echo $p_group_10; ?>"><?php echo $p_group_10; ?></option>
         <option value="<?php echo $p_group_11; ?>"><?php echo $p_group_11; ?></option>
         <option value="<?php echo $p_group_12; ?>"><?php echo $p_group_12; ?></option>
         <option value="<?php echo $p_group_13; ?>"><?php echo $p_group_13; ?></option>
         <option value="<?php echo $p_group_14; ?>"><?php echo $p_group_14; ?></option>
         <option value="<?php echo $p_group_15; ?>"><?php echo $p_group_15; ?></option>
         <option value="<?php echo $p_group_16; ?>"><?php echo $p_group_16; ?></option>
         <option value="<?php echo $p_group_17; ?>"><?php echo $p_group_17; ?></option>
        </select>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">Location</label></td>
      <td width="4%" class="static_head" align="center">--</td>
      <td width="72%" class="dynamic_values">
      <select name="state" id="state"class="select_loc" onChange="getDistrict(this.options[this.selectedIndex].value);">
        <option value="Select"> Select State </option>
        <?php
          while($fetch_state=mysql_fetch_array($run_state)){
        ?>
          <option value="<?php echo $fetch_state['State_ID']; ?>"> <?php echo $fetch_state['State_Name']; ?> </option>
        <?php
          }
        ?>
      </select>
      <select name="district" id="district"class="select_loc"onChange="getCities(this.options[this.selectedIndex].value);"><option value="">Select District</option></select>
      <select name="city" id="city"class="select_loc"><option value="">Select City</option></select>
      </td>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">Phone Number</label></td>
      <td width="4%" class="static_head" align="center">--</td>
      <td width="72%" class="dynamic_values">
          <input type="text" name="m_phone" value="<?php echo $fetch['MOBILE_NUMBER']; ?>" class="textbox_"></td>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">E-Mail</label></td>
      <td width="4%" class="static_head" align="center">--</td>
      <td width="72%" class="dynamic_values">
          <input type="email" name="m_email" value="<?php echo $fetch['E_MAIL']; ?>" class="textbox_"></td>
    </tr>
    <tr>
      <td width="24%" class="static_head"><label class="label" for="name">Influenced Person ID</label></td>
      <td width="4%" class="static_head" align="center">--</td>
      <td width="72%" class="dynamic_values">
          <input type="text" name="m_id" value="<?php echo $fetch['Motivated_Person']; ?>" class="textbox_"></td>
    </tr>

    <tr>
      <td width="72%" class="dynamic_values" colspan="3">
          <input type="hidden" name="m_url" value="<?php echo $c_url; ?>" class="textbox_"></td>
    </tr>

    <tr>
      <td width="20%" class="static_head" colspan="3" align="center"><input type="submit" class="button" value=" Replace My Details "></td>
    </tr>
  </table>
  </form>
 </div>
</div>


<?php
  include('../footer.php');
?>