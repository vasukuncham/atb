<?php
  include('../header.php');
  include('../config.php');

    $count = "SELECT COUNT(Request_ID) AS numrows
              FROM request_list";

    $c_result  = mysql_query($count);
    $c_row     = mysql_fetch_array($c_result);
    $n_rows    = $c_row['numrows'];

    //echo $n_rows;



    $rowsPerPage = 10; // how many rows to show per page
    $pageNum = 1; // by default we show first page

    // if $_GET['page'] defined, use it as page number
    if(isset($_GET['page']))
    {
      $pageNum = $_GET['page'];
    }

    $offset = ($pageNum - 1) * $rowsPerPage; // counting the offset

    $maxPage = ceil($n_rows/$rowsPerPage); // how many pages we have when using paging?

    $self = $_SERVER['PHP_SELF']; // print the link to access each page
    $nav  = '';

    $startPage =$pageNum;
    $lastPage = $maxPage;

    if($startPage+9 < $maxPage)
    {
      $lastPage = $startPage+9;
    }

    for($page_ctr = $startPage; $page_ctr <= $lastPage; $page_ctr++)
    {
      if ($page_ctr == $pageNum)
      {  
        $nav .= " $page_ctr "; // no need to create a link to current page
      }
      else
      {   // <a href="/v8.1/book.php?page=2">2</a>
        $nav .= " <a href=\"$self?page=$page_ctr$SortParam\">$page_ctr</a> ";
      } 
    }

    // creating previous and next link and the link to go straight to the first and last page
    if ($pageNum > 1)
    {
      $page  = $pageNum - 1;
      $prev  = " <a href=\"$self?page=$page$SortParam\"><</a> ";
      $first = " <a href=\"$self?page=1$SortParam\"><<</a> ";
    } 
    else
    {
      $prev  = '&nbsp;'; // we're on page one, don't print previous link
      $first = '&nbsp;'; // nor the first page link
    }

    if ($pageNum < $maxPage)
    {
      $page = $pageNum + 1;
      $next = " <a href=\"$self?page=$page$SortParam\">></a> ";
      $last = " <a href=\"$self?page=$maxPage$SortParam\">>></a> ";
    }
    else
    {
      $next = '&nbsp;'; // we're on the last page, don't print next link
      $last = '&nbsp;'; // nor the last page link
    }

    if($n_rows > 1)
    {
      $p_rows = "Requests";
    }
    else
    {
      $p_rows = "Request";    
    }
    if($maxPage > 1)
    {
      $p_pages = "Pages";
    }
    else
    {
      $p_pages = "Page";
    }

    $select = "SELECT * FROM request_list
               ORDER BY Request_ID DESC
               LIMIT ".$offset.",".$rowsPerPage;
    $result = mysql_query($select);

?>

<div id="main_reg_request">

    <table align = "center" width = "940px" border="0px">
      <tr>
        <td align = "right" width="50%" class-"back"> <a href = "../index.php">Home</a> </td>
      </tr>
    </table>

    <br>

    <?php
      if($n_rows == 0)
      {
    ?>

       <table width = "940px" align="center" border="0px">
        <tr align="center">
         <td> <span style="color:red; font-weight:bold;">There are no Blood Requests.</span></td>
        </tr>
       </table>

<br>

    <?php
      }
      else
      {
    ?>
    

    <table align = "center" width = "940px" border="0px">
      <tr>
        <td class = "nav">
        <?php
            echo $first . $prev . $nav . $next . $last . " &nbsp;&nbsp;&nbsp;
                 [ " . $n_rows . " " . $p_rows." in " . $maxPage . " ".$p_pages. " ]";
        ?>
        </td>
      </tr>
    </table>

  <table align="center" border="0px" class="insert_01">
    <tr align="center">
      <td width="27%" class="admin_head">Patient Name</td>
      <td width="5%" class="admin_head">Age</td>
      <td width="10%" class="admin_head">Blood Group</td>
      <td width="11%" class="admin_head">Request Date</td>
      <td width="20%" class="admin_head">Location</td>
      <td width="27%" class="admin_head">Hospital</td>
    </tr>
  <?php
    while($row = mysql_fetch_array($result))
    {
  ?>
    <tr class="req_data">
      <td width="26%" class="admin_data"><?php echo $row['Patient_Name']; ?></td>
      <td width="5%" class="admin_data"><?php echo $row['Patient_Age']; ?></td>
      <td width="12%" class="admin_data"><?php echo $row['Blood_Group']; ?></td>
      <td width="12%" class="admin_data"><?php echo $row['Req_of_Last_Date']; ?></td>
      <td width="20%" class="admin_data"><?php echo $row['Location']; ?></td>
      <td width="25%" class="admin_data"><?php echo $row['Hospital_Name']; ?></td>
    </tr>
   <?php
     }
   ?>
  </table>

    <table align = "center" width = "940px" border="0px">
      <tr>
        <td class = "nav_b">
        <?php
            echo $first . $prev . $nav . $next . $last . " &nbsp;&nbsp;&nbsp;
                 [ " . $n_rows . " " . $p_rows." in " . $maxPage . " ".$p_pages. " ]";
        ?>
        </td>
      </tr>
    </table>
  <?php
   }
  ?>
</div>

<?php
  include('../footer.php');
?>
