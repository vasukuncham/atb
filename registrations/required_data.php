<style>

body
{
 background: none;
 padding: 0px;
 margin: 0 auto;
 text-align: center;
}

#main
{
 width: 100%;
 position: relative;
 margin: 0 auto;
 background: none;
}
.request
{
 width: 40%;
 margin: 0 auto;
 border: none;
 margin-top: 5px;
 text-align: left;
}
.textbox
 {
   font-family: Arial, Helvetica, sans-serif;
   background: #FFF; /*rgba(255, 255, 255, 0.44)*/ 
   color: #000;
   border: 1px solid #A4A4A4;
   padding: 3px 3px 3px 10px;
   font-size: 15px;
   width: 280px;
   height:30px;
 }
.textbox:hover
{
   border: 1px solid #EFC01D;
   box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
   -moz-box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
   -webkit-box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
 }
.textbox:focus
 {
   border: 1px solid #EFBB1D;
   outline: none;
   box-shadow: inset 0px 1px 2px rgba(0,0,0,0.3);
   -moz-box-shadow: inset 0px 1px 2px rgba(0,0,0,0.3);
   -webkit-box-shadow: inset 0px 1px 2px rgba(0,0,0,0.3);
   background: rgb(255, 255, 255);
 }

</style>


<div id="main">
  <div class="request">
    <span style="color: blue; font-weight: bold;">Name : </span>
    <input type="text" name="r_name" class="textbox" placeholder="Enter Your Full Name">
  </div>

  <div class="request">
    <span style="color: blue; font-weight: bold;">Mobile Number : </span>
    <input type="text" name="r_name" class="textbox" placeholder="Enter Your Mobile Number">
  </div>

  <div class="request">
    <span style="color: blue; font-weight: bold;">E-Mail : </span>
    <input type="text" name="r_name" class="textbox" placeholder="Enter Your E-Mail">
  </div>
</div>