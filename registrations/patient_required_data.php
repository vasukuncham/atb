<?php

include('../header.php');
include('../config.php');

$p_blood_group  = $_GET['bgroup'];
$p_location     = $_GET['loc'];

// echo "Selected Blood Group is: ".$p_blood_group."</br>";
// echo "Location is: ".$p_location."</br>";
?>

  <style type="text/css">
    @import "../css/jquery.datepick.css";
  </style>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.datepick.js"></script>

  <script type="text/javascript">
    $(function()
    {
      $('#popupDatepicker').datepick();
      $('#popupDatepicker_01').datepick();
      $('#inlineDatepicker').datepick({onSelect: showDate});
    }
    );
  </script>

  <script>

    function Focus()
    {
      document.patient.p_name.focus();
    }

    function validate()
    {
      if(document.patient.p_name.value == "")
      {
        alert("Please Enter the Patient Full Name");
        document.patient.p_name.focus();
        return false;
      }
      if(document.patient.p_age.value == "")
      {
        alert("Please Enter the Patient Age in Numbers");
        document.patient.p_age.focus();
        return false;
      }
      if(document.patient.l_date.value == "")
      {
        alert("Please Select the date");
        document.patient.l_date.focus();
        return false;
      }
      if(document.patient.p_phone.value == "")
      {
        alert("Please Enter Your Phone Number");
        document.patient.p_phone.focus();
        return false;
      }
      if(document.patient.h_name.value == "")
      {
        alert("Please Enter Which Hospital the Patient was Joined");
        document.patient.h_name.focus();
        return false;
      }
      if(document.patient.reason.value == "")
      {
        alert("Please Enter Reason to Getting the Blood");
        document.patient.reason.focus();
        return false;
      }
    }
   </script>
<?php
 if($p_otp == $c_otp)
 {
 // echo "OTP Successfully Created"
?>

  <body onLoad="Focus()">

      <table align="center" width="70%" border="0px"> 
        <tr>
          <td align="right"><a href = "req_existing_blood.php" class="home_">Back</a></td>
        </tr>
      </table>

<br>

 <div id="main_reg">
  <div class="sub_main">
  <form action="../otp/send_data.php" method="POST" name="patient" enctype = "multipart/form-data" onsubmit="return validate()">

   <input type="hidden" name="blood_group" value="<?php echo $p_blood_group; ?>">
   <input type="hidden" name="location" value="<?php echo $p_location; ?>">

   <table width=90%" border="0px" align="center">
     <tr>
       <td width="30%"><label class="label">Patient Name</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="p_name" class="textbox_" placeholder="Full Name"></td>
     </tr>
     <tr>
       <td width="30%"><label class="label">Patient Age</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="p_age" class="textbox_" placeholder="In Numbers"></td>
     </tr>
     <tr>
       <td width="30%"><label class="label">Last date for getting blood</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="l_date" class="textbox_" placeholder="Last Date" id="popupDatepicker"></td>
     </tr>
     <tr>
       <td width="30%"><label class="label">My Cell Number</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="p_phone" class="textbox_" placeholder="Mobile Number" id="popupDatepicker"></td>
     </tr>
     <tr>
       <td width="30%"><label class="label">Hospital Name</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="h_name" class="textbox_" placeholder="Hospital Name"></td>
     </tr>
     <tr>
       <td width="30%"><label class="label">Hospital Phone Number</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="h_phone_no" class="textbox_" placeholder="Hospital Phone Number"></td>
     </tr>
     <tr>
       <td width="30%" valign="center"><label class="label">Need of Blood</label></td>
       <td width="5%"><span style="color:red;font-weight:bold">--</span></td>
       <td width="65%" align="right"><input type="text" name="reason" class="textbox_" placeholder="Ex:- Accidents, Delivery, Surgery...etc."></td>
     </tr>
     <tr>
       <td width="25%" colspan="3" align="center">
     <div class="submit">
      <input type="submit" name="submit" class="button" value=" Submit "></div>
       </td>
     </tr>
   </table>
  </form>
  </div>
 </div>

<?php
}
else
{
      print "<script>";
      print "alert('Your Entered OTP Number is not Match. Please Tray Again');";
      print "self.location='../registrations/existing_blood.php';";
      print "</script>";
}
?>



<?php
  include('../footer.php');
?>