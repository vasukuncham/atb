<?php
  include('../header.php');
  include('../config.php');

  $select = "SELECT DISTINCT G_ID,G_Name FROM b_group ORDER BY G_ID";
  $run    = mysql_query($select);

 $select_state = "SELECT State_ID,State_Name FROM state";
 $run_state    = mysql_query($select_state);
?>

<style>

body
{
 background: none;
 padding: 0px;
 margin: 0 auto;
 text-align: center;
}

.existing_blood
{
 margin: 0 auto;
 border: none;
 margin-top: 1em;
 margin-bottom: 1em;
 padding: 0 1em 0 0;
}
.textbox
 {
   font-family: Arial, Helvetica, sans-serif;
   background: #FFF; /*rgba(255, 255, 255, 0.44)*/ 
   color: #000;
   border: 1px solid #A4A4A4;
   padding: 3px 3px 3px 10px;
   font-size: 15px;
   width: 270px;
   height:30px;
 }
.textbox:hover
{
   border: 1px solid #EFC01D;
   box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
   -moz-box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
   -webkit-box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
 }
.textbox:focus
 {
   border: 1px solid #EFBB1D;
   outline: none;
   box-shadow: inset 0px 1px 2px rgba(0,0,0,0.3);
   -moz-box-shadow: inset 0px 1px 2px rgba(0,0,0,0.3);
   -webkit-box-shadow: inset 0px 1px 2px rgba(0,0,0,0.3);
   background: rgb(255, 255, 255);
 }
.sub_main
{
 margin: 0 auto;
 width: 98%;
 text-align: left;
 margin-bottom: 1em;
 border: none;
}
.request
{
 width: 40%;
 margin: 0 auto;
 border: none;
 margin-top: 5px;
 text-align: left;
}
.text_width
{
 width: 25%;
 color: blue;
 font-weight: bold;
 border: none;
}
.next_level
{
 margin-top: 1em;
}
</style>

<?php
 $Count="10";
?>

  <script>

    function validate()
    {
      if(document.ext_blood.blood_group.value == "Select")
      {
        alert("Please Select Blood Group Which You Want");
        document.ext_blood.blood_group.focus();
        return false;
      }
      if(document.ext_blood.location.value == "Select")
      {
        alert("Please Select Your Nearest Location");
        document.ext_blood.location.focus();
        return false;
      }
    }
</script>

  <script>
        function getDistrict(stateID){
        var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            var res=xmlhttp.responseText;
            // alert(res);
            document.getElementById("district").innerHTML=res;
            }
          }
        xmlhttp.open("GET","second_script.php?stateID="+stateID+"&ref=district", true);
        xmlhttp.send();
        }

        function getCities(cityID){
        var xmlhttp;
        if (window.XMLHttpRequest)
          {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
          }
        else
          {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
          }
        xmlhttp.onreadystatechange=function()
        {
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            var res=xmlhttp.responseText;
            // alert(res);
            document.getElementById("city").innerHTML=res;
            }
          }
        xmlhttp.open("GET","second_script.php?cityID="+cityID+"&ref=city", true);
        xmlhttp.send();
        }

</script>

<div id="main">

      <table align="center" width="70%" border="0px"> 
        <tr>
          <td align="right"><a href = "../index.php"class="home_">Home</a></td>
        </tr>
      </table>

  <div class="existing_blood">
  <form action="req_existing_blood.php" method="POST" name="ext_blood" enctype= "multipart/form-data">
    <!-- <span style="color: blue; font-weight: bold;">Blood Groups : </span> -->
  <select name="blood_group" id="blood_group" class="select">
    <option value="Select">Select Blood Group</option>
    <?php
      while($fetch  = mysql_fetch_array($run)){
    ?>
      <option value="<?php echo $fetch['G_Name']; ?>"><?php echo $fetch['G_Name']; ?></option>
    <?php
      }
    ?>
  </select>
      <select name="state" id="state"class="select" onChange="getDistrict(this.options[this.selectedIndex].value);">
        <option value="Select"> -----Select State----- </option>
        <?php
          while($fetch_state=mysql_fetch_array($run_state)){
        ?>
          <option value="<?php echo $fetch_state['State_ID']; ?>"> <?php echo $fetch_state['State_Name']; ?> </option>
        <?php
          }
        ?>
      </select>
     <select class="select" id="district" name="district" onChange="getCities(this.options[this.selectedIndex].value);"><option value="">-----Select District-----</option></select>
     <select class="select" id="city" name="city" onChange="getDonars(this.options[this.selectedIndex].value);"><option value="">-----Select City-----</option></select>
     <input type="submit" name="submit" class="button_loc" value=" Search ">
  </form>
    <div id="address"></div>

  </div>
</div>


<?php
  include('../footer.php');
?>