<?php
 session_start();
 if(!$_SESSION['atb'])
 {
  print "<script>";
  print "self.location = 'logout.php'";
  print "</script>";
 }

 include('header.php');
 include('../config.php');
?>

<!DOCTYPE html>
<!-- saved from url=(0062)http://voky.com.ua/showcase/sky-forms/examples/ext-icons.html? -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Life Savers Registrations</title>


  <style type="text/css">
    @import "../css/jquery.datepick.css";
  .style1 {color: #FF0000}
  </style>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.datepick.js"></script>

  <script type="text/javascript">
    $(function()
    {
      $('#popupDatepicker').datepick();
      $('#popupDatepicker_01').datepick();
      $('#inlineDatepicker').datepick({onSelect: showDate});
    }
    );
  </script>

  <script>

    function Focus()
    {
      document.register.name.focus();
    }

    function validate()
    {
      if(document.register.name.value == "")
      {
        alert("Please Enter the Your Full Name");
        document.register.name.focus();
        return false;
      }
      if(document.register.gender.value == "Select")
      {
        alert("Please Select Your Gender");
        document.register.gender.focus();
        return false;
      }
      if(document.register.dob.value == "")
      {
        alert("Please Enter Your Age");
        document.register.dob.focus();
        return false;
      }
      if(document.register.b_group.value == "Select")
      {
        alert("Please Select Your Blood Group");
        document.register.b_group.focus();
        return false;
      }
      if(document.register.phone.value == "")
      {
        alert("Please Enter Your Phone Number");
        document.register.phone.focus();
        return false;
      }
      if(document.register.loc.value == "Select")
      {
        alert("Please Select Your Nearest Location");
        document.register.loc.focus();
        return false;
      }
    }
  </script>
	</head>
  <body onLoad="Focus()">

    <br />

		<div id="main_reg">

			<form action="admin_confirmation.php" method="POST" name="register" id="855294376"enctype = "multipart/form-data" onSubmit="return validate()">

      <div align="center"><span style="color:green; margin-top:20px;"><b>Registration form for Saving A Life</b></span></div>
      <table border="0px" width="75%" align="center" cellpadding="0px" cellspacing="0px">
        <tr>
          <td width="30%">
            <div class="input_space"><label class="label" for="name">Enter your Name</label>
            <span class="style1">*</span></div></td>
          <td width="70%" align="right"">
            <div class="input_space"><input type="text" placeholder="Enter your name" name="name" class="textbox_" id="name"></div>
          </td>
        </tr>
        <tr>
          <td>
            <div class="input_space"><label class="label" for="group">Gender</label>
            <span class="style1">*</span></div>
          </td>
          <td width="70%" align="right">
      <div class="input_space">
      <select name="gender" width="3" id="group"class="textbox_">
       <option value="Select"> Select </option>
       <option value="Male"> Male </option>
       <option value="Fe-Male"> Fe-Male </option>
      </select>
      </div>
          </td>
        </tr>
        <tr>
          <td>
      <div class="input_space">
        <label class="label">Age *</label>
      </div>
          </td>
          <td width="70%" align="right">
      <div class="input_space"><input type="text" placeholder="Enter Your Age" name ="p_age" id="popupDatepicker_" class="textbox_">
		  </div>
          </td>
        </tr>
        <tr>
          <td>
      <div class="input_space"><label class="label" for="group">Blood Group</label>
        <span class="style1">*</span></div></td>
          <td width="70%" align="right">
      <div class="input_space">
        <select name="b_group" width="3" id="b_group"class="textbox_">
          <option value="Select"> Select </option>
          <option value="A+"> A+ </option>
          <option value="A-"> A- </option>
          <option value="B+"> B+ </option>
          <option value="B-"> B- </option>
          <option value="O+"> O+ </option>
          <option value="O-"> O- </option>
          <option value="AB+"> AB+ </option>
          <option value="AB-"> AB- </option>
          <option value="A1+"> A1+ </option>
          <option value="A1-"> A1- </option>
          <option value="A2+"> A2+ </option>
          <option value="A2-"> A2- </option>
          <option value="A1B+"> A1B+ </option>
          <option value="A1B-"> A1B- </option>
          <option value="A2B+"> A2B+ </option>
          <option value="A2B-"> A2B- </option>
          <option value="BB"> BOMBAY BLOOD GROUP </option>
          
          </select>
      </div></td>
        </tr>
        <tr>
          <td>
      <div class="input_space"><label class="label" for="email">Email</label></div></td>
          <td width="70%" align="right">
      <div class="input_space"><input type="email" placeholder="If You want to Enter E-Mail Type Currect E-Mail" name="email" class="textbox_" id="email"></div>
          </td>
        </tr>
        <tr>
          <td>
      <div class="input_space"><label class="label" for="id">Reference Donor ID</label></div></td>
          <td width="70%" align="right">
		  <div class="input_space"><input type="text" placeholder="Existing Donor ID" name="valentier" class="textbox_" id="id">
		  </div></td>
		    </tr>
		    <tr>
		      <td>

			<div class="input_space"><label class="label" for="phone">Phone number</label>
			  <span class="style1">*</span></div></td>
			    <td width="70%" align="right">
      <div class="input_space"><input type="tel" placeholder="Enter your Phone number" name="phone" class="textbox_" id="phone">
      </div></td>
        </tr>
        <tr>
          <td>
      <div class="input_space"><label class="label" for="loc">Location</label>
        <span class="style1">* </span></div></td>
          <td width="70%" align="right">
      <div class="input_space">
      <select name="loc" id="loc"class="textbox_">
       <option value="Select"> Select </option>
       <option value="Chirala"> Chirala </option>
       <option value="Vijayawada"> Vijayawada </option>
       <option value="Guntur"> Guntur </option>
       <option value="Ongole"> Ongole </option>
       <option value="Bapatla"> Bapatla </option>
       <option value="Visakapatnam"> Visakapatnam </option>
       <option value="Ponnur"> Ponnur </option>
       <option value="Narsarao Peta"> Narsarao Peta </option>
       <option value="Vizianagaram"> Vizianagaram </option>
       <option value="Kakinada"> Kakinada </option>
       <option value="Bhimavaram"> Bhimavaram </option>
       <option value="Gudipueli"> Gudipueli </option>
       <option value="Hyderabad"> Hyderabad </option>
      </select>
      </div></td>
        </tr>
<!--
		    <tr>
		      <td>

			<div class="input_space"><label class="label" for="phone">Upload Your Picture</label>
			  <span class="style1"></span></div></td>
			    <td width="70%" align="right">
      <div class="input_space"><input type="file" name="file"></div></td>
        </tr>
-->
      </table>

     <div class="submit">
      <input type="submit" class="button" value=" Submit ">
		  <input type="reset" class="button button-secondary" value=" Refresh "></div>

</form>

</div>
	

</body>
</html>


<?php
  include('../footer.php');
?>