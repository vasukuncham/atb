<?php
 session_start();
 if(!$_SESSION['atb'])
 {
  print "<script>";
  print "self.location = 'logout.php'";
  print "</script>";
 }

 include('header.php');
 include('../config.php');
/*
 $select = "SELECT * FROM admin_pwd";
 $run    = mysql_query($select);
 $fetch  = mysql_fetch_array($run);
 $p_pass = $fetch['Password'];

  // echo "Password is:".$p_pass;
*/
?>

  <style type="text/css">
    @import "../css/jquery.datepick.css";
  .style1 {color: #FF0000}
  </style>

  <script type="text/javascript" src="../js/jquery.js"></script>
  <script type="text/javascript" src="../js/jquery.datepick.js"></script>

  <script type="text/javascript">
    $(function()
    {
      $('#popupDatepicker').datepick();
      $('#popupDatepicker_01').datepick();
      $('#inlineDatepicker').datepick({onSelect: showDate});
    }
    );
  </script>

  <script>

    function Focus()
    {
      document.change_pass.a_name.focus();
    }

    function validate()
    {
      if(document.change_pass.a_name.value=="")
      {
        alert("Please Enter Event Name");
        document.change_pass.a_name.focus();
        return false;
      }
      if(document.change_pass.date.value=="")
      {
        alert("Please Select Event Date");
        document.change_pass.date.focus();
        return false;
      }
      if(document.change_pass.e_place.value=="")
      {
        alert("Please Enter Event Place");
        document.change_pass.e_place.focus();
        return false;
      }
    }
  </script>
<br><br>
<div id="main_reg">

 <div class="sub_main_space">
 <form action="album_p.php" method="POST" name="change_pass" "enctype = "multipart/form-data" onsubmit="return validate()">

      <table border="0px" width="75%" align="center" cellpadding="0px" cellspacing="0px">
        <tr>
          <td width="30%">
            <div class="input_space"><label class="label" for="name">Event Name</label>
            <span class="style1">*</span></div></td>
          <td width="70%" align="right"">
            <div class="input_space"><input type="text" placeholder="Album Name" name="a_name" class="textbox_" id="name"></div>
          </td>
        </tr>

        <tr>
          <td>
      <div class="input_space">
        <label class="label">Date *</label>
      </div>
          </td>
          <td width="70%" align="right">
      <div class="input_space"><input type="text" placeholder="Select Album Date" name ="date" id="popupDatepicker" class="textbox_">
		  </div>
          </td>
        </tr>

        <tr>
          <td>
      <div class="input_space"><label class="label" for="email">Place *</label></div></td>
          <td width="70%" align="right">
      <div class="input_space"><input type="text" placeholder="Event Place" name="e_place" class="textbox_" id="email"></div>
          </td>
        </tr>

        <tr>
          <td>
      <div class="input_space"><label class="label" for="email">Description *</label></div></td>
          <td width="70%" align="right">
      <div class="input_space">
       <textarea name="des" class="textarea"></textarea>
      </div>
          </td>
        </tr>

		    <tr>
		      <td>

			<div class="input_space"><label class="label" for="image">Select Images</label>
			  <span class="style1">*</span></div></td>
			    <td width="70%" align="right">
      <div class="input_space"><input name='uploads[]' type='file' multiple='multiple' id="image"/>
      </div></td>
        </tr>
      </table>
     <div class="submit">
      <input type="submit" class="button" value=" Submit "> &nbsp;&nbsp;
		  <input type="reset" class="button button-secondary" value=" Refresh "></div>
 </form>
 </div>
</div>


<?php
  include('../footer.php');
?>