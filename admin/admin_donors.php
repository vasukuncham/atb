<?php
 session_start();
 if(!$_SESSION['atb'])
 {
  print "<script>";
  print "self.location = 'logout.php'";
  print "</script>";
 }

  include('header.php');
  include('../config.php');
/*
  $loc_count_d        = "SELECT DISTINCT LOCATION FROM atbblooddonar";
  $run_count_d        = mysql_query($loc_count_d);
  $fetch_count_d      = mysql_fetch_array($run_count_d);
  $Count_total_loc_d  = $fetch_count_d['LOCATION'];
*/

    $count = "SELECT COUNT(ATB_D_NO) AS numrows
              FROM atbblooddonar";

    $c_result  = mysql_query($count);
    $c_row     = mysql_fetch_array($c_result);
    $n_rows    = $c_row['numrows'];

    //echo $n_rows;

  $url = $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"]; // $_SERVER['SCRIPT_FILENAME']; 
  
  // echo "The Current Pags URL is:".$url."<br>";

    $rowsPerPage = 20; // how many rows to show per page
    $pageNum = 1; // by default we show first page

    // if $_GET['page'] defined, use it as page number
    if(isset($_GET['page']))
    {
      $pageNum = $_GET['page'];
    }

    $offset = ($pageNum - 1) * $rowsPerPage; // counting the offset

    $maxPage = ceil($n_rows/$rowsPerPage); // how many pages we have when using paging?

    $self = $_SERVER['PHP_SELF']; // print the link to access each page
    $nav  = '';

    $startPage =$pageNum;
    $lastPage = $maxPage;

    if($startPage+9 < $maxPage)
    {
      $lastPage = $startPage+9;
    }

    for($page_ctr = $startPage; $page_ctr <= $lastPage; $page_ctr++)
    {
      if ($page_ctr == $pageNum)
      {  
        $nav .= " $page_ctr "; // no need to create a link to current page
      }
      else
      {   // <a href="/v8.1/book.php?page=2">2</a>
        $nav .= " <a href=\"$self?page=$page_ctr$SortParam\">$page_ctr</a> ";
      } 
    }

    // creating previous and next link and the link to go straight to the first and last page
    if ($pageNum > 1)
    {
      $page  = $pageNum - 1;
      $prev  = " <a href=\"$self?page=$page$SortParam\"><</a> ";
      $first = " <a href=\"$self?page=1$SortParam\"><<</a> ";
    } 
    else
    {
      $prev  = '&nbsp;'; // we're on page one, don't print previous link
      $first = '&nbsp;'; // nor the first page link
    }

    if ($pageNum < $maxPage)
    {
      $page = $pageNum + 1;
      $next = " <a href=\"$self?page=$page$SortParam\">></a> ";
      $last = " <a href=\"$self?page=$maxPage$SortParam\">>></a> ";
    }
    else
    {
      $next = '&nbsp;'; // we're on the last page, don't print next link
      $last = '&nbsp;'; // nor the last page link
    }


  if($n_rows > 1)
  {
    $p_rows = "Donors";
  }
  else
  {
    $p_rows = "Donor";    
  }
  if($maxPage > 1)
  {
    $p_pages = "Pages";
  }
  else
  {
    $p_pages = "Page";
  }

  $select = "SELECT * FROM atbblooddonar
             LIMIT ".$offset.",".$rowsPerPage;
  $result = mysql_query($select);


?>

<style>
body
{
  width: 100%;
  padding: 0px;
  margin: 0 auto;
  background: none;
}
#main
{
  width: 80%;
  margin: 0 auto;
  margin-top: 1em;
  border: none;
  padding: 0px;
}

.redius_slider1
{
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 2px solid #727272;
}
.padding_menus
{
  padding: 0 1em 1em 1em;
  color: red;
  font-weight: bold;
  font-size: 17px;
}
.padding_head
{
  vertical-align: center;
  padding: 10px;
  color: red;
  font-weight: bold;
  font-size: 17px;
}
.padding_general
{
  vertical-align: center;
  padding: 0px;
  color: blue;
  font-weight: none;
  font-size: 15px;
}
.padding
{
  vertical-align: center;
  padding: 5px;
  color: blue;
  font-weight: none;
  font-size: 15px;
}
.delete
{
  background: #C0DBF4;
}
</style>
<br>
<div align="center">
  <div id="main">

    <?php
      if($n_rows == "0")
      {
    ?>
    <table border="0px" width="100%" align="center" cellspacing="0px" cellpadding="0px">
      <tr>
        <td>Donors not available.</td>
      </tr>
    </table>
    <?php  
      }
      else
      {
    ?>

    <table align = "center" width = "98%" border="0px">
      <tr>
        <td class = "nav">
        <?php
            echo $first . $prev . $nav . $next . $last . " &nbsp;&nbsp;&nbsp;
                 [ " . $n_rows . " " . $p_rows." in " . $maxPage . " ".$p_pages. " ]";
        ?>
        </td>
      </tr>
    </table>

    <br>

    <table border="1px" width="100%" align="center" cellspacing="0px" cellpadding="0px">
      <tr align="center">
        <td class="padding_head" width="10%">Donor ID</td>
        <td class="padding_head" width="23%">Name</td>
        <td class="padding_head" width="5%">Group</td>
        <td class="padding_head" width="12%">Mobile Num...</td>
        <td class="padding_head" width="17%">Location</td>
        <td class="padding_head" width="14%">Date</td>
        <td class="padding_head" width="6%">Modify</td>
        <td class="padding_head" width="7%">Status</td>
        <td class="padding_head" width="6%">Delete</td>
      </tr>
   <?php
     while($fetch = mysql_fetch_array($result))
     {
      $city = $fetch['LOCATION'];
      $select_city = "SELECT C_Name FROM city WHERE C_ID='$city'";
      $run_city    = mysql_query($select_city);
      $fetch_city  = mysql_fetch_array($run_city);
     if($fetch['Status'] == "Delete")
     {
   ?>

      <tr class="delete">
        <td class="padding_general" align="center">
            <a href="donor_details.php?d_id=<?php echo $fetch['ATB_D_NO']; ?> && name=<?php echo $fetch['NAME']; ?>">
            <?php echo $fetch['ATB_D_NO']; ?></a></td>
        <td class="padding"><?php echo $fetch['NAME']; ?></td>
        <td class="padding_general" align="center"><?php echo $fetch['BLOOD_GROUP']; ?></td>
        <td class="padding_general" align="center"><?php echo $fetch['MOBILE_NUMBER']; ?></td>
        <td class="padding"><?php echo $fetch_city['C_Name']; ?></td>
        <td class="padding_general" align="center"><?php echo $fetch['Reg_Date']; ?></td>
        <td class="padding_general" align="center"><a href="donor_modify.php?d_id=<?php echo $fetch['ATB_D_NO']; ?>"><img src="../site_pictures/modify.png" width="25px"></a></td>
        <td class="padding_general" align="center">
            <a href="donor_status.php?d_id=<?php echo $fetch['ATB_D_NO']; ?> && status=<?php echo $fetch['Status']; ?>">
            <?php echo $fetch['Status']; ?></a></td>
        <td class="padding_general" align="center"><a href="donor_del.php?d_id=<?php echo $fetch['ATB_D_NO']; ?>&&url=<?php echo $url; ?>"><img src="../site_pictures/delete.png" width="25px"></a></td>
      </tr>
   <?php
     }
     else
     {
   ?>
      <tr>
        <td class="padding_general" align="center">
            <a href="donor_details.php?d_id=<?php echo $fetch['ATB_D_NO']; ?> && name=<?php echo $fetch['NAME']; ?>">
            <?php echo $fetch['ATB_D_NO']; ?></a></td>
        <td class="padding"><?php echo $fetch['NAME']; ?></td>
        <td class="padding_general" align="center"><?php echo $fetch['BLOOD_GROUP']; ?></td>
        <td class="padding_general" align="center"><?php echo $fetch['MOBILE_NUMBER']; ?></td>
        <td class="padding"><?php echo $fetch_city['C_Name']; ?></td>
        <td class="padding_general" align="center"><?php echo $fetch['Reg_Date']; ?></td>
        <td class="padding_general" align="center"><a href="donor_modify.php?d_id=<?php echo $fetch['ATB_D_NO']; ?>"><img src="../site_pictures/modify.png" width="25px"></a></td>
        <td class="padding_general" align="center">
            <a href="donor_status.php?d_id=<?php echo $fetch['ATB_D_NO']; ?> && status=<?php echo $fetch['Status']; ?>">
            <?php echo $fetch['Status']; ?></a></td>
        <td class="padding_general" align="center"><a href="donor_del.php?d_id=<?php echo $fetch['ATB_D_NO']; ?>&&url=<?php echo $url; ?>"><img src="../site_pictures/delete.png" width="25px"></a></td>
      </tr>
   <?php
     }
     }
   ?>
    </table>

    <br>

    <table align = "center" width = "98%" border="0px">
      <tr>
        <td class = "nav">
        <?php
            echo $first . $prev . $nav . $next . $last . " &nbsp;&nbsp;&nbsp;
                 [ " . $n_rows . " " . $p_rows." in " . $maxPage . " ".$p_pages. " ]";
        ?>
        </td>
      </tr>
    </table>

  </div>
</div>

<?php
    }
  include('../footer.php');
?>