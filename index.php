<?php
  include('./index_header.php');
  include('./config.php');

  $loc_count_d        = "SELECT DISTINCT LOCATION FROM atbblooddonar";
  $run_count_d        = mysql_query($loc_count_d);
  $fetch_count_d      = mysql_fetch_array($run_count_d);
  $Count_total_loc_d  = $fetch_count_d['LOCATION'];

  $count_total_loc = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE LOCATION='$Count_total_loc_d'";
  $c_result_total_loc  = mysql_query($count_total_loc);
  $c_row_total_loc     = mysql_fetch_array($c_result_total_loc);
  $Count_total_loc     = $c_row_total_loc['num'];

  $count_total_d = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar";
  $c_result_total_d  = mysql_query($count_total_d);
  $c_row_total_d     = mysql_fetch_array($c_result_total_d);
  $Count_total_d     = $c_row_total_d['num'];

  $count_total_r = "SELECT COUNT(Request_ID) AS num
                     FROM request_list";
  $c_result_total_r  = mysql_query($count_total_r);
  $c_row_total_r     = mysql_fetch_array($c_result_total_r);
  $Count_total_r     = $c_row_total_r['num'];

  $count_a_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A+'";
  $c_result_a_p  = mysql_query($count_a_p);
  $c_row_a_p     = mysql_fetch_array($c_result_a_p);
  $Count_a_p     = $c_row_a_p['num'];

  $count_a_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A-'";
  $c_result_a_n  = mysql_query($count_a_n);
  $c_row_a_n     = mysql_fetch_array($c_result_a_n);
  $Count_a_n     = $c_row_a_n['num'];

  $count_b_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='B+'";
  $c_result_b_p  = mysql_query($count_b_p);
  $c_row_b_p     = mysql_fetch_array($c_result_b_p);
  $Count_b_p     = $c_row_b_p['num'];

  $count_b_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='B-'";
  $c_result_b_n  = mysql_query($count_b_n);
  $c_row_b_n     = mysql_fetch_array($c_result_b_n);
  $Count_b_n     = $c_row_b_n['num'];

  $count_o_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='O+'";
  $c_result_o_p  = mysql_query($count_o_p);
  $c_row_o_p     = mysql_fetch_array($c_result_o_p);
  $Count_o_p     = $c_row_o_p['num'];

  $count_o_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='O-'";
  $c_result_o_n  = mysql_query($count_o_n);
  $c_row_o_n     = mysql_fetch_array($c_result_o_n);
  $Count_o_n     = $c_row_o_n['num'];

  $count_ab_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='AB+'";
  $c_result_ab_p  = mysql_query($count_ab_p);
  $c_row_ab_p     = mysql_fetch_array($c_result_ab_p);
  $Count_ab_p     = $c_row_ab_p['num'];

  $count_ab_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='AB-'";
  $c_result_ab_n  = mysql_query($count_ab_n);
  $c_row_ab_n     = mysql_fetch_array($c_result_ab_n);
  $Count_ab_n     = $c_row_ab_n['num'];

  $count_a1_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A1+'";
  $c_result_a1_p  = mysql_query($count_a1_p);
  $c_row_a1_p     = mysql_fetch_array($c_result_a1_p);
  $Count_a1_p     = $c_row_a1_p['num'];

  $count_a1_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A1-'";
  $c_result_a1_n  = mysql_query($count_a1_n);
  $c_row_a1_n     = mysql_fetch_array($c_result_a1_n);
  $Count_a1_n     = $c_row_a1_n['num'];

  $count_a2_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A2+'";
  $c_result_a2_p  = mysql_query($count_a2_p);
  $c_row_a2_p     = mysql_fetch_array($c_result_a2_p);
  $Count_a2_p     = $c_row_a2_p['num'];

  $count_a2_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A2-'";
  $c_result_a2_n  = mysql_query($count_a2_n);
  $c_row_a2_n     = mysql_fetch_array($c_result_a2_n);
  $Count_a2_n     = $c_row_a2_n['num'];

  $count_a1b_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A1B+'";
  $c_result_a1b_p  = mysql_query($count_a1b_p);
  $c_row_a1b_p     = mysql_fetch_array($c_result_a1b_p);
  $Count_a1b_p     = $c_row_a1b_p['num'];

  $count_a1b_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A1B-'";
  $c_result_a1b_n  = mysql_query($count_a1b_n);
  $c_row_a1b_n     = mysql_fetch_array($c_result_a1b_n);
  $Count_a1b_n     = $c_row_a1b_n['num'];

  $count_a2b_p = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A2B+'";
  $c_result_a2b_p  = mysql_query($count_a2b_p);
  $c_row_a2b_p     = mysql_fetch_array($c_result_a2b_p);
  $Count_a2b_p     = $c_row_a2b_p['num'];

  $count_a2b_n = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='A2B-'";
  $c_result_a2b_n  = mysql_query($count_a2b_n);
  $c_row_a2b_n     = mysql_fetch_array($c_result_a2b_n);
  $Count_a2b_n     = $c_row_a2b_n['num'];

  $count_ab = "SELECT COUNT(ATB_D_NO) AS num
                     FROM atbblooddonar WHERE BLOOD_GROUP='BB'";
  $c_result_ab  = mysql_query($count_ab);
  $c_row_ab     = mysql_fetch_array($c_result_ab);
  $count_ab     = $c_row_ab['num']

?>

<style>
body
{
  padding: 0px;
  margin: 0 auto;
  background: none;
}
#main
{
  margin: 0 auto;
  margin-top: 1em;
  padding: 0px;
}
.total_list
{
  height: 150px;
  border: 2px solid #727272;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
.slider_
{
  width: 100%;
  margin: 0 auto;
  height: 200px;
  margin-bottom: 3em;
  background: none;
  border : none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}

.redius
{
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
.redius_slider
{
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 2px solid #727272;
}
.border_redius
{
  border: 2px solid #727272;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
.total_donors
{

 background: none;
}
.total_req
{
 width: 100%;
 height: 150px;
 background: none;
}
.total_visit
{
 width: 100%;
 height: 100px;
 margin-top: 4em;
 background: none;
}
.donor_style
{
  margin-top: 0.3em;
  text-indent: 4px;
  font-size:15px;
}
.donor_style_head
{
  padding: 0.2em 0 0 0;
}
.add
{
  height: 150px;
  border: 2px solid #727272;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
.footer
{
  width: 100%;
  height: 30px;
  background: #c0c0c0;
  font-size: 14px;
  padding: 10px;
  margin-top: 2em;
}

.redius_slider1 {    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 2px solid #727272;
}
.slides1 {  width: 95%;
  margin: 0 auto;
  height: 200px;
  margin-bottom: 3em;
  background: none;
  border : none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
}
.mar
{
  margin: 0 auto;
}
</style>

<script type="text/javascript" src="./js/cufon-yui_.js"></script>
<script type="text/javascript" src="./js/droid_sans_400-droid_sans_700.font_.js"></script>
<script type="text/javascript" src="./js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="./js/script.js"></script>
<script type="text/javascript" src="./js/coin-slider.min.js"></script>

<div align="center">
<div id="main">
  <table width="1050px" align="center" border="0px" cellspacing="0px" cellpadding="0px">
    <tr class="mar">
      <td width="20%" align="center" valign="top"><a href=""><img src="site_pictures/picture_1.jpg" width="190px"class="redius" /></a></td>
      <td width="60%"rowspan="2" valign="top">
        <table width="500px" align="center" border="0px">
          <tr>
            <td colspan="4"align="center">
              <!-- <div class="slides"><span class="slides1"><img src="site_pictures/picture_4N.jpg" width="550"class="redius_slider1" /></span></div> -->
              <div class="slider">
                <div id="coin-slider">
                  <!-- <a href="#"><img src="./site_pictures/slide1.jpg" width="570" height="160" alt="Image - 1" /></a> -->
                  <a href="#"><img src="./site_pictures/slide2.jpg" width="570" height="160" alt="Image - 2" /></a>
                  <a href="#"><img src="./site_pictures/slide3.jpg" width="570" height="160" alt="Image - 3" /></a>

                </div>
                <div class="clr"></div>
              </div>
            </td>
          </tr>
          <tr>
            <td align="center" style="padding-top:1em"><a href="./registrations/register.php"><img src="./site_pictures/register.jpg" width="80%"  class="redius"></a></td>
            <td align="center" style="padding-top:1em"><a href="./otp/login_profile.php"><img src="./site_pictures/login.jpg" width="80%" class="redius"></a></td>
            <td align="center" style="padding-top:1em"><a href="./registrations/existing_blood.php"><img src="./site_pictures/request.jpg" width="80%" class="redius"></a></td>
          </tr>
        </table>      </td>
      <td width="20%" valign="top"rowspan="2" class="border_redius">
        <div class="total_donors">
        
        <div class="donor_style"><span style="color:green; font-weight:bold;">Total Donors Available: </span>807</div>
        <div class="donor_style"><span style="color:green; font-weight:bold;">No of Locations: </span><?php echo $Count_total_loc; ?></div>
        <table align="center" border="0px">
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A+: </span>28</div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A-: </span>11</div></td>
          </tr>
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">B+: </span>59</div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">B-: </span>22</div></td>
          </tr>
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">O+: </span>50</div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">O-: </span>13</div></td>
          </tr>
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">AB+: </span>7</div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">AB-: </span>13</div></td>
          </tr>
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A1+: </span>3</div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;"> A1-: </span><?php echo $Count_a1_n; ?></div></td>
          </tr>
          <tr>
             <td><div class="donor_style"><span style="color:green; font-weight:bold;">A2+: </span><?php echo $Count_a2_p; ?></div></td>
             <td><div class="donor_style"><span style="color:green; font-weight:bold;">A2-: </span><?php echo $Count_a2_n; ?></div></td>
          </tr>
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A1B+: </span><?php echo $Count_a1b_p; ?></div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A1B-: </span><?php echo $Count_a1b_n; ?></div></td>
          </tr>
          <tr>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A2B+: </span><?php echo $Count_a2b_p; ?></div></td>
            <td><div class="donor_style"><span style="color:green; font-weight:bold;">A2B-: </span><?php echo $Count_a2b_n; ?></div></td>
          </tr>
          <tr>
            <td colspan="2"><div class="donor_style"><span style="color:green; font-weight:bold;">Bombay Blood Group: </span><?php echo $count_ab; ?></div></td>
          </tr>
          <tr>
            <td colspan="2"><div class="donor_style"><span style="color:green; font-weight:bold;">Total Requests: </span>34</div></td>
          </tr>
          <tr>
            <td colspan="2"><div class="donor_style"><span style="color:green;">Total Visitors: </span>1148</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td width="20%" align="center"><a href="https://www.youtube.com/watch?v=gr9USvwctpA" target="_blank"><img src="https://i.ytimg.com/vi_webp/gr9USvwctpA/mqdefault.webp" width="190px"class="redius_slider" ></a></td>
    </tr>
  </table>

</div>
</div>

<?php
  include('footer.php');
?>