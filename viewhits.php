<html>
<head>
<title>CodeLair Hit Counter - version 2.0</title>
<link rel="stylesheet" type="text/css" href="tablestyle.css" />
</head>

<body>

<?php

$filename = "hitcount.txt";
$i = 0;

echo '<h1>CodeLair Hit Counter</h1>';
// check that there is something in the file
if ( filesize( $filename ) > 0 )
{
	// get list of pages from file
	$handle = fopen($filename, 'r');
	while ( !feof( $handle ) )
	{
		$buffer = trim( fgets( $handle ) );
		if ( strlen( $buffer ) > 0 )
		{
			list( $pages[$i], $count[$i] ) = explode( '|', $buffer );
			$total += $count[$i];
			$i++;
		}
	}
	fclose($handle);

	// sort list
	array_multisort($count,SORT_DESC,$pages);


	// display results
	echo '<p class="info">TOTAL HITS<br /><b>' . number_format( $total ) . '</b></p>';

	echo '<table>';
	echo '<tr><th>Filename</th><th>Hits</th></tr>';

	for ( $j = 0; $j < $i; $j++ )
	{
		echo '<tr>';
		echo '<td class="text">' . $pages[$j] . '</td>';
		echo '<td class="number">' . number_format( $count[$j] ) . '</td>';
		echo '</tr>';
	}
	echo '</table>';
}
else
{
	echo '<p class="info">No statistics recorded yet!</p>';
}

echo '<p class="info">PHP hit counter script by <a href="http://www.doheth.co.uk/codelair">CodeLair PHP scripts</a></p>';

// Google ad - this is here for extra support, if it annoys you feel free to take it out.
echo '<p>&nbsp;</p>';
echo '<p align="center">
  <script type="text/javascript"><!--
  google_ad_client = "pub-4942990752504243";
  google_alternate_ad_url = "http://www.doheth.co.uk/adsense.html";
  google_ad_width = 728;
  google_ad_height = 90;
  google_ad_format = "728x90_as";
  google_ad_channel ="5416907455";
  google_color_link = "000000";
  google_color_bg = "FFFFFF";
  google_color_url = "666666";
  google_color_border = "CCCCCC";
  google_color_text = "333333";
  //--></script>
  <script type="text/javascript"
    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
  </script></p>';

?>

</body>
</html>